﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MODELO;

namespace WEB.Controllers
{
    public class ConfiguracionEmailController : Controller
    {
        notifixEntities ne = new notifixEntities();
        public ActionResult NuevoAjusteEMAIL()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevoAjusteEMAIL(string txtNomConfigEmail, string txtEmail, string txtClave, string txtHost, int txtPuerto, string txtCertificado)
        {
            DateTime Hoy = DateTime.Today;
            string fecha_actual = Hoy.ToString("dd-MM-yyyy");
            bool valor;

            if (txtCertificado == "1")
            { valor = true; }
            else
            { valor = false; }

            try
            {
                ne.ntx_NuevaConfEmail(txtNomConfigEmail, txtEmail, txtClave, txtHost, txtPuerto, fecha_actual, valor);
                ViewBag.Mensaje = "ok";
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Mensaje = "no";
                return View();
            }
            
        }
    }
}