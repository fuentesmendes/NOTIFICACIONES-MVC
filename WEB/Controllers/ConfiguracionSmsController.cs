﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MODELO;

namespace WEB.Controllers
{
    public class ConfiguracionSmsController : Controller
    {
        notifixEntities ne = new notifixEntities();
        // GET: ConfiguracionSms
        public ActionResult NuevoAjusteSMS()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevoAjusteSMS(string txtNomConfigSms, string txtUrl, string txtMetodo)
        {
            DateTime Hoy = DateTime.Today;
            string fecha_actual = Hoy.ToString("dd-MM-yyyy");

            try
            {
                ne.ntx_NuevaConfSms(txtNomConfigSms, txtUrl, txtMetodo, fecha_actual);
                ViewBag.mensaje = "ok";
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.mensaje = "No";
                return View();
            }
        }
    }
}