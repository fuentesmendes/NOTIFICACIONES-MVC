﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webSMSAxede.Helper;
using webSMSAxede.Models;

namespace WEB.Controllers
{
    public class SMSController : Controller
    {
        private GestionRegistros _registros = null;

        private List<ContactoModel> pObtenerListado
        {
            get
            {
                _registros = new GestionRegistros();
                List<ContactoModel> objLista = _registros.pObtenerListado;
                _registros = null;
                return objLista;
            }
            set
            {
                _registros = new GestionRegistros();
                _registros.pObtenerListado = value;
                _registros = null;
            }
        }

        public JsonResult ObtenerListado()
        {
            List<ContactoModel> listContacto = pObtenerListado.ToList();

            return Json(listContacto, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SmsGeneral()
        {
            string msg = "";
            if (Session["mensaje"] != null)
                msg = Session["mensaje"].ToString();
            return View("SmsGeneral", pObtenerListado);
        }

        public ActionResult SmsIndividual()
        {
            string msg = "";
            if (Session["mensaje"] != null)
                msg = Session["mensaje"].ToString();
            return View("SmsIndividual", pObtenerListado);
        }

        public JsonResult Eliminar(string id, string msg)
        {
            try
            {
                if (Session["mensaje"] == null)
                {
                    Session["mensaje"] = msg;

                }
                else
                {
                    if (Session["mensaje"].ToString() != msg)
                    {
                        Session["mensaje"] = msg;
                    }
                }

                List<ContactoModel> listContacto = pObtenerListado;
                var registro = listContacto.FirstOrDefault(k => k.Celular == id);
                if (registro != null)
                {
                    listContacto.Remove(registro);
                    pObtenerListado = listContacto;
                }
                var respuesta = new { estado = "ok", mensaje = "" };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception error)
            {
                Reportar("Adicionar", "error: " + error.Message);
                var respuesta = new { estado = "nok", mensaje = error.Message };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }

        }

        //Inicio Adicionar SMS-GENERAL
        public JsonResult Adicionar(ContactoModel contacto)
        {
            try
            {


                if (Session["mensaje"] == null)
                {
                    Session["mensaje"] = contacto.msg;

                }
                else
                {
                    if (Session["mensaje"].ToString() != contacto.msg)
                    {
                        Session["mensaje"] = contacto.msg;
                    }
                }

                if (pObtenerListado.Any(k => k.Celular == contacto.Celular)) throw new Exception("Ya existe un contacto con el número celular: " + contacto.Celular + ".\nPor favor verifique!.");
                List<ContactoModel> listContacto = pObtenerListado;
                contacto.Enviar = true;
                listContacto.Add(contacto);
                pObtenerListado = listContacto;
                var respuesta = new { estado = "ok", mensaje = "" };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception error)
            {
                Reportar("Adicionar", "error: " + error.Message);
                var respuesta = new { estado = "nok", mensaje = error.Message };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }
        //Fin Adicionar SMS-GENERAL

        //Inicio de Adicionar SMS-INDIVIDUAL
        public JsonResult AdicionarIndiv(ContactoModel contacto)
        {
            try
            {


                if (Session["mensaje"] == null)
                {
                    Session["mensaje"] = contacto.msg;

                }
                else
                {
                    if (Session["mensaje"].ToString() != contacto.msg)
                    {
                        Session["mensaje"] = contacto.msg;
                    }
                }

                if (pObtenerListado.Any(k => k.Celular == contacto.Celular)) throw new Exception("Ya existe un contacto con el número celular: " + contacto.Celular + ".\nPor favor verifique!.");
                List<ContactoModel> listContacto = pObtenerListado;
                contacto.Enviar = true;
                listContacto.Add(contacto);
                pObtenerListado = listContacto;
                var respuesta = new { estado = "ok", mensaje = "" };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception error)
            {
                Reportar("AdicionarIndiv", "error: " + error.Message);
                var respuesta = new { estado = "nok", mensaje = error.Message };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }
        //Fin de Adicionar SMS-INDIVIDUAL

        public JsonResult ActualizarEnviar(string id, bool estado)
        {
            try
            {
                List<ContactoModel> listContacto = pObtenerListado;
                var registro = listContacto.FirstOrDefault(k => k.Celular == id);
                if (registro != null)
                {
                    registro.Enviar = estado;
                    pObtenerListado = listContacto;
                }
                var respuesta = new { estado = "ok", mensaje = "" };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception error)
            {
                Reportar("ActualizarEnviar", "error: " + error.Message);
                var respuesta = new { estado = "nok", mensaje = error.Message };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CargarArchivo()
        {
            Reportar("CargarArchivo", "Ingresa");
            try
            {

                ProcesarArchivo();
                var respuesta = new { estado = "ok", mensaje = "" };
                TempData["MensajeInformativo"] = "Carga correcta!";
                Reportar("CargarArchivo", "termina");
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception error)
            {
                Reportar("CargarArchivo", "error: " + error.Message);
                var respuesta = new { estado = "nok", mensaje = error.Message };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult EnviarMensajes(string texto)
        {
            try
            {
                List<ContactoModel> listContacto = pObtenerListado;
                if (listContacto.Count == 0) throw new Exception("no existen contáctos para enviar mensajes!");
                var qLista = pObtenerListado.Where(k => k.Enviar).ToList();
                ProcesarMensajes(qLista, texto);
                pObtenerListado = new List<ContactoModel>();
                var respuesta = new { estado = "ok", mensaje = "" };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception error)
            {
                Reportar("EnviarMensajes", "error: " + error.Message);
                var respuesta = new { estado = "nok", mensaje = error.Message };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        private void ProcesarMensajes(List<ContactoModel> lsContactos, string texto)
        {
            string Contenido;
            string baseContenido = "{0} {1} {2}";
            lsContactos.ForEach(delegate (ContactoModel item)
            {
                if (lsContactos.Count <= 1 && texto != "")
                {
                    Contenido = string.Format(baseContenido, item.Tratamiento.ToUpper(), item.Nombre.ToUpper(), texto.Trim());
                }
                else if (lsContactos.Count > 1 && texto != "")
                {
                    Contenido = string.Format(baseContenido, item.Tratamiento.ToUpper(), item.Nombre.ToUpper(), texto.Trim());
                }
                else if (lsContactos.Count <= 1 && texto == "")
                {
                    Contenido = string.Format(baseContenido, item.Tratamiento.ToUpper(), item.Nombre.ToUpper(), item.msg.Trim());
                }
                else if(lsContactos[0].msg != lsContactos[1].msg || texto == "")
                {                     
                    Contenido = string.Format(baseContenido, item.Tratamiento.ToUpper(), item.Nombre.ToUpper(), item.msg.Trim());
                }
                else
                {
                    Contenido = string.Format(baseContenido, item.Tratamiento.ToUpper(), item.Nombre.ToUpper(), texto.Trim());
                }




                webSMSAxede.Helper.GestionMensaje objGestion = new webSMSAxede.Helper.GestionMensaje();
                objGestion.Enviar(item.Celular, Contenido);
                objGestion = null;
            }
            );
        }

        private void ProcesarArchivo()
        {
            string RutaArchivo = "";
            try
            {

                #region MyRegion
                try
                {
                    HttpPostedFileBase objArchivo = Request.Files[0];
                    string NombreArchivo = objArchivo.FileName;
                    System.IO.Stream fileContent = objArchivo.InputStream;
                    RutaArchivo = Server.MapPath("~/Uploads/") + pNombreArchivo + System.IO.Path.GetExtension(NombreArchivo);
                    objArchivo.SaveAs(RutaArchivo);
                    objArchivo = null;
                    fileContent.Dispose();
                }
                catch (Exception e)
                {
                    throw new Exception("problemas al cargar el archivo al servidor: " + e.Message);
                }
                #endregion

                #region MyRegion
                try
                {
                    string ExtensionArchivo = System.IO.Path.GetExtension(RutaArchivo); string ConexionArchivo = "";
                    if (ExtensionArchivo.Equals(".xls"))
                    {
                        ConexionArchivo = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + RutaArchivo + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        ConexionArchivo = string.Format(pConexionExcel2003, RutaArchivo);
                    }
                    else if (ExtensionArchivo.Equals(".xlsx"))
                    {
                        ConexionArchivo = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + RutaArchivo + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        ConexionArchivo = string.Format(pConexionExcel2007, RutaArchivo);
                    }

                    OleDbConnection excelConnection = new OleDbConnection(ConexionArchivo);
                    excelConnection.Open();
                    DataTable dt = new DataTable();
                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null) throw new Exception("Archivo inválido");
                    String[] vecHojas = new String[dt.Rows.Count];
                    int t = 0;
                    foreach (DataRow row in dt.Rows)
                    {
                        vecHojas[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    string query = string.Format("Select Nombre,Tratamiento,Celular from [{0}]", vecHojas[0]);
                    dt = new DataTable();
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection))
                    {
                        dataAdapter.Fill(dt);
                        List<ContactoModel> listContacto = (from item in dt.AsEnumerable()
                                                            select new ContactoModel
                                                            {
                                                                Celular = item.Field<double>("Celular").ToString(),
                                                                Nombre = item.Field<string>("Nombre"),
                                                                Tratamiento = item.Field<string>("Tratamiento"),
                                                                Enviar = true
                                                            }).ToList();
                        var qLista = pObtenerListado;
                        qLista.AddRange(listContacto);
                        pObtenerListado = qLista;
                        dataAdapter.Dispose();
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("problemas al leer la información: " + ex.Message);
                }
                #endregion

            }
            catch (Exception error)
            {
                throw error;
            }

        }

        //Inicio de Cargar Archivo SMS-GENERAL GET
        public ActionResult CargaArchivo()
        {
            string msg = Request.QueryString["msg"];
            if (Session["mensaje"] == null)
            {
                Session["mensaje"] = msg;

            }
            else
            {
                if (Session["mensaje"].ToString() != msg)
                {
                    Session["mensaje"] = msg;
                }
            }

            return View();
        }
        //Fin de Cargar Archivo SMS-GENERAL GET

        //Inicio de Cargar Archivo SMS-GENERAL POST
        [HttpPost]
        public ActionResult CargaArchivo(HttpPostedFileBase postedFile)
        {

            Reportar("CargarArchivo", "Ingresa");
            HttpPostedFileBase objArchivo = Request.Files[0];
            try
            {

                ProcesarArchivo(objArchivo);
                Reportar("CargarArchivo", "termina");
            }
            catch (Exception error)
            {
                Reportar("CargarArchivo", "error: " + error.Message);
            }
            return RedirectToAction("SmsGeneral", "SMS");
        }
        //Fin de Cargar Archivo SMS-GENERAL POST


        //Inicio de Cargar Archivo SMS-INDIVIDUAL GET
        public ActionResult CargaArchivoInd()
        {
            string msg = Request.QueryString["msg"];
            if (Session["mensaje"] == null)
            {
                Session["mensaje"] = msg;

            }
            else
            {
                if (Session["mensaje"].ToString() != msg)
                {
                    Session["mensaje"] = msg;
                }
            }

            return View();
        }
        //Fin de Cargar Archivo SMS-INDIVIDUAL GET

        //Inicio de Cargar Archivo SMS-INDIVIDUAL POST
        [HttpPost]
        public ActionResult CargaArchivoInd(HttpPostedFileBase postedFile)
        {

            Reportar("CargarArchivo", "Ingresa");
            HttpPostedFileBase objArchivo = Request.Files[0];
            try
            {

                ProcesarArchivoInd(objArchivo);
                Reportar("CargaArchivoInd", "termina");
            }
            catch (Exception error)
            {
                Reportar("CargaArchivoInd", "error: " + error.Message);
            }
            return RedirectToAction("SmsIndividual", "SMS");
        }
        //Fin de Cargar Archivo SMS-INDIVIDUAL POST

        private void ProcesarArchivo(HttpPostedFileBase ArchivoCarga)
        {
            string RutaArchivo = "";
            try
            {
                #region MyRegion
                try
                {
                    if (!(Request.Files[pNombreControl].ContentLength > 0)) throw new Exception("no existe un archivo para procesar");

                    var NombreArchivo = Path.GetFileName(ArchivoCarga.FileName);


                    RutaArchivo = Server.MapPath(pDirectorioCarga) + pNombreArchivo + System.IO.Path.GetExtension(NombreArchivo);
                    if (System.IO.File.Exists(RutaArchivo)) System.IO.File.Delete(RutaArchivo);
                    Request.Files[pNombreControl].SaveAs(RutaArchivo);
                }
                catch (Exception exArchivo)
                {
                    throw new Exception("error al cargar el archivo: " + exArchivo.Message);
                }
                #endregion

                #region MyRegion
                try
                {
                    string CadenaConexion = string.Empty; string ExtensionArchivo = System.IO.Path.GetExtension(RutaArchivo);
                    DataTable dt = new DataTable(); int Indice = 0;

                    if (ExtensionArchivo == ".xls")
                        CadenaConexion = string.Format(pConexionExcel2003, RutaArchivo);
                    else if (ExtensionArchivo == ".xlsx")
                        CadenaConexion = string.Format(pConexionExcel2007, RutaArchivo);

                    OleDbConnection ConexionArchivo = new OleDbConnection(CadenaConexion);
                    ConexionArchivo.Open();

                    dt = ConexionArchivo.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null) throw new Exception("archivo inválido");

                    String[] vecHojas = new String[dt.Rows.Count];
                    foreach (DataRow row in dt.Rows)
                    {
                        vecHojas[Indice] = row["TABLE_NAME"].ToString();
                        Indice++;
                    }

                    string ConsultaArchivo = string.Format("Select Nombre,Tratamiento,Celular from [{0}]", vecHojas[0]);

                    dt = new DataTable();
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(ConsultaArchivo, ConexionArchivo))
                    {
                        dataAdapter.Fill(dt);
                        List<ContactoModel> listContacto = (from item in dt.AsEnumerable()
                                                            let lista = pObtenerListado
                                                            where lista.Any(k => k.Celular == item.Field<double>("Celular").ToString()) == false
                                                            select new ContactoModel
                                                            {
                                                                Celular = item.Field<double>("Celular").ToString(),
                                                                Nombre = item.Field<string>("Nombre"),
                                                                Tratamiento = item.Field<string>("Tratamiento"),
                                                                Enviar = true
                                                            }).ToList();
                        var qLista = pObtenerListado;
                        qLista.AddRange(listContacto);
                        pObtenerListado = qLista;
                        dataAdapter.Dispose();
                    }
                    dt.Dispose();
                }
                catch (Exception exLectura)
                {
                    throw new Exception("error al leer el archivo: " + exLectura.Message);
                }
                #endregion
            }
            catch (Exception error)
            {
                Reportar("ProcesarArchivo", "error: " + error.Message);
                throw error;
            }
        }

        //Inicio de procesar archivo individual
        private void ProcesarArchivoInd(HttpPostedFileBase ArchivoCarga)
        {
            string RutaArchivo = "";
            try
            {
                #region MyRegion
                try
                {
                    if (!(Request.Files[pNombreControl].ContentLength > 0)) throw new Exception("no existe un archivo para procesar");

                    var NombreArchivo = Path.GetFileName(ArchivoCarga.FileName);


                    RutaArchivo = Server.MapPath(pDirectorioCarga) + pNombreArchivo + System.IO.Path.GetExtension(NombreArchivo);
                    if (System.IO.File.Exists(RutaArchivo)) System.IO.File.Delete(RutaArchivo);
                    Request.Files[pNombreControl].SaveAs(RutaArchivo);
                }
                catch (Exception exArchivo)
                {
                    throw new Exception("error al cargar el archivo: " + exArchivo.Message);
                }
                #endregion

                #region MyRegion
                try
                {
                    string CadenaConexion = string.Empty; string ExtensionArchivo = System.IO.Path.GetExtension(RutaArchivo);
                    DataTable dt = new DataTable(); int Indice = 0;

                    if (ExtensionArchivo == ".xls")
                        CadenaConexion = string.Format(pConexionExcel2003, RutaArchivo);
                    else if (ExtensionArchivo == ".xlsx")
                        CadenaConexion = string.Format(pConexionExcel2007, RutaArchivo);

                    OleDbConnection ConexionArchivo = new OleDbConnection(CadenaConexion);
                    ConexionArchivo.Open();

                    dt = ConexionArchivo.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null) throw new Exception("archivo inválido");

                    String[] vecHojas = new String[dt.Rows.Count];
                    foreach (DataRow row in dt.Rows)
                    {
                        vecHojas[Indice] = row["TABLE_NAME"].ToString();
                        Indice++;
                    }

                    string ConsultaArchivo = string.Format("Select Nombre,Tratamiento,Celular,Correo,AsuntoCorreo,NumContrato,NomContacto,TelContacto,Actividad from [{0}]", vecHojas[0]);

                    dt = new DataTable();
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(ConsultaArchivo, ConexionArchivo))
                    {
                        dataAdapter.Fill(dt);
                        List<ContactoModel> listContacto = (from item in dt.AsEnumerable()
                                                            let lista = pObtenerListado
                                                            where lista.Any(k => k.Celular == item.Field<double>("Celular").ToString()) == false
                                                            select new ContactoModel
                                                            {
                                                                Celular = item.Field<double>("Celular").ToString(),
                                                                Nombre = item.Field<string>("Nombre").ToString(),
                                                                Tratamiento = item.Field<string>("Tratamiento").ToString(),
                                                                Email = item.Field<string>("Correo").ToString(),
                                                                Actividad = item.Field<string>("Actividad").ToString(),
                                                                AsuntoEmail = item.Field<string>("AsuntoCorreo").ToString(),
                                                                NumContrato = Convert.ToString(item.Field<string>("NumContrato")),
                                                                NomContacto = item.Field<string>("NomContacto").ToString(),
                                                                TelContacto = item.Field<double>("TelContacto").ToString(),
                                                                msg = "contrato de " + item.Field<string>("Actividad").ToString() + " de la plataforma " + item.Field<string>("NumContrato").ToString() + " se vence en los proximos dias. Favor comunicarse con " + item.Field<string>("NomContacto").ToString() + " al telefono " + item.Field<double>("TelContacto").ToString(),
                                                                Enviar = true
                                                            }).ToList();
                        var qLista = pObtenerListado;
                        qLista.AddRange(listContacto);
                        pObtenerListado = qLista;
                        dataAdapter.Dispose();
                    }
                    dt.Dispose();
                }
                catch (Exception exLectura)
                {
                    throw new Exception("error al leer el archivo: " + exLectura.Message);
                }
                #endregion
            }
            catch (Exception error)
            {
                Reportar("ProcesarArchivoInd", "error: " + error.Message);
                throw error;
            }
        }
        //Fin de procesar archivo individual

        [HttpPost]
        public JsonResult LimpiarTabla()
        {
            pObtenerListado = new List<ContactoModel>();
            var respuesta = new { estado = "ok", mensaje = "" };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        private void Reportar(string Metodo, string Mensaje)
        {
            try
            {
                GestionLog.Escribir(Metodo, Mensaje);
            }
            catch (Exception)
            {

            }
        }

        private string pNombreArchivo
        {
            get
            {
                return string.Format(@"{0}", Guid.NewGuid());
            }
        }
        private string pDirectorioCarga
        {
            get { return ConfigurationManager.AppSettings["directorio_carga"]; }
        }
        private string pNombreControl
        {
            get { return ConfigurationManager.AppSettings["nombre_controlfile"]; }
        }
        private string pConexionExcel2007
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConexionProveedorExcel2007"].ConnectionString;
            }
        }
        private string pConexionExcel2003
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConexionProveedorExcel2003"].ConnectionString;
            }
        }

    }

}