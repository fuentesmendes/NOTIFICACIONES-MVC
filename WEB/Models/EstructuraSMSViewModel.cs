﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webSMSAxede.Models
{
  public class EstructuraSMSViewModel
  {
    public bool Enviar { set; get; }
    public string NroCelular { set; get; }
    public string NombreContacto { set; get; }
  }
}