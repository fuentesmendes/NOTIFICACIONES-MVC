﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webSMSAxede.Models
{
  public class ContactoModel
  {
    public string ID { get; private set; }
    public string Nombre { get; set; }
    public string Celular { get; set; }
    public string Email { get; set; }
    public string Actividad { get; set; }
    public string AsuntoEmail { get; set; }
    public string NumContrato { get; set; }
    public string NomContacto { get; set; }
    public string TelContacto { get; set; }
    public bool Enviar { get; set; }
    public string Tratamiento { get; set; }
    public string Respuesta { get; set; }
    public string msg { get; set; }
        public ContactoModel()
    {      
      Random rdn = new Random();
      ID = rdn.Next(Int32.MaxValue).ToString();
    }
  }
}