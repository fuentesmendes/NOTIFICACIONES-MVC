﻿function clearTextBox() {
    $('#divrespuesta').val('');
    $('#EmployeeID').val("");
    $('#Name').val("");
    $('#Age').val("");
    $('#State').val("");
    $('#Country').val("");
    $('#btnUpdate').hide();
    $('#btnAdd').show();
    $('#Name').css('border-color', 'lightgrey');
    $('#Age').css('border-color', 'lightgrey');
    $('#State').css('border-color', 'lightgrey');
    $('#Country').css('border-color', 'lightgrey');
}
//***************************************************************************
function validate() {
    var isValid = true;
    if ($('#nombre').val().trim() == "") {
        $('#nombre').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#nombre').css('border-color', 'lightgrey');
    }
    var control = $('#correo');
    if (control.val().trim() == "") {
        control.css('border-color', 'Red');
        isValid = false;
    }
    else {
      control.css('border-color', 'lightgrey');
    }
    control = $('#tratamiento');
    if (control.val().trim() == "") {
      control.css('border-color', 'Red');
      isValid = false;
    }
    else {
      control.css('border-color', 'lightgrey');
    }    

    return isValid;
}