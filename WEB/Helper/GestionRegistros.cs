﻿using System.Collections.Generic;
using webSMSAxede.Models;

namespace webSMSAxede.Helper
{
  public class GestionRegistros
  {
    private const string _idListado = "idlistado";
    public List<ContactoModel> pObtenerListado
    {
      get
      {
        List<ContactoModel> objLista =
         System.Web.HttpContext.Current.Session[_idListado] == null ?
          new List<ContactoModel>() :
          System.Web.HttpContext.Current.Session[_idListado] as List<ContactoModel>;
        return objLista;
      }
      set
      {
        System.Web.HttpContext.Current.Session[_idListado] = value;
        List<ContactoModel> objLista = System.Web.HttpContext.Current.Session[_idListado] as List<ContactoModel>;
      }
    }
  }
}