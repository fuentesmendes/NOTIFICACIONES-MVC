﻿using System;
using System.IO;
using System.Web;

namespace webSMSAxede.Helper
{
  public class GestionLog
  {
    public static void Escribir(string Metodo, string Mensaje)
    {
      try
      {
        string baseRutaLog = @"{0}{1}.txt";
        string RutaLog = string.Format(baseRutaLog, HttpContext.Current.Server.MapPath("~/app_data/log/"), DateTime.Now.ToString("yyyy-MM-dd"));
        using (StreamWriter sw = new StreamWriter(RutaLog, true))
        {
          sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
          sw.WriteLine("Metodo:{0}", Metodo);
          sw.WriteLine("Mensaje:{0}", Mensaje);
          sw.WriteLine("***************************************");
          sw.Close();
        }
        
      }
      catch (Exception )
      {
        
      }
    }
  }
}