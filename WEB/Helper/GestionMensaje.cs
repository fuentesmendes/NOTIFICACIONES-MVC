﻿using System;
using System.Net;
using System.Web;
using MODELO;
using System.Linq;

namespace webSMSAxede.Helper
{
  public class GestionMensaje
  {
        //notifixEntities ne = new notifixEntities();
    public void Enviar(string NroCelular, string Mensaje)
    { 

      try
      {          

           string url = string.Format(urlSMS, NroCelular, Mensaje);
           WebRequest webReq = WebRequest.Create(url);
           WebResponse webRes = webReq.GetResponse();
      }
      catch (Exception error)
      {
        throw new Exception("Error al enviar el mensaje");
      }
    }
    private string urlSMS
    {

       
      get
      {
        string url = "";
        try
        {
            //var idSms = (from c in ne.t_sms select c).Max(c => c.Id);
            //var urlBd = (from s in ne.t_sms
            //             where s.Id == idSms
            //             select s.Url).First();
                    string rutaParametro = HttpContext.Current.Server.MapPath("~/app_data/Configuracion/parametro_url.txt");
                    //string rutaParametro = urlBd.Trim();
                    System.IO.StreamReader sr = new System.IO.StreamReader(rutaParametro);
            if (sr.Peek() > 0) url = sr.ReadToEnd();
            sr.Close();
        }
        catch (Exception error)
        {
          throw new Exception("no se halló parámetro de envío de mensajes: " + error.Message);
        }
        return url;
      }
    }
  }
}